const user = require('./user');
const userbiodata = require('./userbiodata');
const userhistory = require('./userhistory');

module.exports = {
    user,
    userbiodata,
    userhistory,
};
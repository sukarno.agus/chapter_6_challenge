const express = require("express");
const router = express();

const userController = require('../controllers').user;
const userbiodataController = require('../controllers').userbiodata;
const userhistoryController = require('../controllers').userhistory;

router.use(express.Router());
router.use(express.json());
router.use(express.urlencoded({ extended: true }));

router.get("/", (req, res, next) => {
  const name = req.query.name;
  res.render("index", { title: "Traditional Games", name: name });
});

router.post("/", (req, res, next) => {
  const user = req.query;
  console.log(user);

  res.render("index", { title: "Traditional Games", name: user.name });
});

/* User Router */
router.get('/api/user', userController.list);
router.get('/api/user/:id', userController.getById);
router.post('/api/user', userController.add);
router.put('/api/user/:id', userController.update);
router.delete('/api/user/:id', userController.delete);

/* UserBiodata Router */
router.get('/api/userbiodata', userbiodataController.list);
router.get('/api/userbiodata/:id', userbiodataController.getById);
router.post('/api/userbiodata', userbiodataController.add);
router.put('/api/userbiodata/:id', userbiodataController.update);
router.delete('/api/userbiodata/:id', userbiodataController.delete);

/* UserHistory Router */
router.get('/api/userhistory', userhistoryController.list);
router.get('/api/userhistory/:id', userhistoryController.getById);
router.post('/api/userhistory', userhistoryController.add);
router.put('/api/userhistory/:id', userhistoryController.update);
router.delete('/api/userhistory/:id', userhistoryController.delete);


module.exports = router;
